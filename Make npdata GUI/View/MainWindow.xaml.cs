﻿using Make_npdata_GUI.Model;
using Microsoft.Win32;
using Shared_Classes;
using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace Make_npdata_GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Make_npdata _make_npdata = new Make_npdata();

        private const string RAPS_FOLDER = "raps/";
        private const string BIN_KLIC = "klic.bin";
        private const string TXT_KLIC = "klics.txt";

        private Shared_Classes.DKlic_Finder dklic_finder = new Shared_Classes.DKlic_Finder();

        public MainWindow()
        {
            INIFile ini = new INIFile("settings.ini", false, true);

            string language = ini.GetValue("General", "Language", "en");

            Thread.CurrentThread.CurrentUICulture = new CultureInfo(language);
            Thread.CurrentThread.CurrentCulture = new CultureInfo(language);
            CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo(language);
            CultureInfo.DefaultThreadCurrentCulture = new CultureInfo(language);

            InitializeComponent();

            string[] languages = new string[] { "es", "en", "pl", "ru" };

            for (int i = 0; i < languages.Length; i++)
                if (languages[i] == language)
                {
                    (languageMI.Items[i] as MenuItem).IsChecked = true;
                    break;
                }

            if (language == "ru")
                Width = 705;

            if (!File.Exists("make_npdata.exe"))
            {
                MessageBox.Show(Properties.Resources.ErrNoMakeNP, Properties.Resources.Title, MessageBoxButton.OK, MessageBoxImage.Error);

                Environment.Exit(1);
            }

            task.SelectedIndex = 1;
        }

        private void InputButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = Properties.Resources.Title;
            ofd.Filter = task.SelectedIndex != 2 ? Properties.Resources.ofdFilterXdat : Properties.Resources.ofdFilterDat;
            ofd.CheckFileExists = false;
            ofd.FileName = Properties.Resources.SelectFolder;

            if (ofd.ShowDialog().Value)
            {
                if (!File.Exists(ofd.FileName))
                {
                    int index = ofd.FileName.LastIndexOf(@"\");
                    ofd.FileName = ofd.FileName.Substring(0, index);
                }

                input.Text = ofd.FileName;

                if (task.SelectedIndex == 0)
                    source.Text = File.Exists(input.Text) ? input.Text.Remove(input.Text.LastIndexOf(@"\") + 1) + "EBOOT.ELF" : input.Text + @"\EBOOT.ELF";
                else if (task.SelectedIndex == 1)
                    output.Text = File.Exists(input.Text) ? input.Text + ".dat" : input.Text;
                else if (task.SelectedIndex == 2)
                    output.Text = File.Exists(input.Text) ? input.Text.Remove(input.Text.Length - 4) : input.Text;

                if (input.Text.ToLower().EndsWith(".edat"))
                {
                    byte[] klic = dklic_finder.GetKlic(ofd.FileName, TXT_KLIC);

                    if (klic != null)
                    {
                        dklic.Text = BitConverter.ToString(klic).Replace("-", "");
                        File.WriteAllBytes(BIN_KLIC, klic);
                    }
                }

                ScrollToEnd();
            }
        }

        private void SourceButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = Properties.Resources.Title;
            ofd.Filter = Properties.Resources.ofdFilterElf;

            if (ofd.ShowDialog().Value)
            {
                source.Text = ofd.FileName;

                ScrollToEnd();
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = Properties.Resources.Title;
            sfd.CheckFileExists = false;
            sfd.FileName = Properties.Resources.SelectFolder;

            if (sfd.ShowDialog().Value)
            {
                if (sfd.FileName.Contains(Properties.Resources.SelectFolder))
                {
                    int index = sfd.FileName.LastIndexOf(@"\");
                    sfd.FileName = sfd.FileName.Substring(0, index);
                }

                if (Directory.Exists(input.Text) && Directory.Exists(sfd.FileName))
                    output.Text = sfd.FileName;
                else if (File.Exists(input.Text) && !Directory.Exists(sfd.FileName))
                    output.Text = sfd.FileName;

                ScrollToEnd();
            }
        }

        private void ScrollToEnd()
        {
            input.ScrollToHorizontalOffset(input.Text.Length * 5);
            source.ScrollToHorizontalOffset(source.Text.Length * 5);
            output.ScrollToHorizontalOffset(output.Text.Length * 5);
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(input.Text) && (byte)task.SelectedIndex == 0)
            {
                MessageBox.Show(Properties.Resources.ErrNoInputFile, Properties.Resources.Title, MessageBoxButton.OK, MessageBoxImage.Error);

                return;
            }
            else if (string.IsNullOrWhiteSpace(input.Text))
            {
                MessageBox.Show(Properties.Resources.ErrNoInputFileOrFolder, Properties.Resources.Title, MessageBoxButton.OK, MessageBoxImage.Error);

                return;
            }

            Title += " - Working...";
            //OutputTextBox.Text = OutputTextBox.Text.Insert(OutputTextBox.CaretIndex, "Working...");

            Start_Work();

            OutputTextBox.Focus();
            OutputTextBox.CaretIndex = OutputTextBox.Text.Length;
            OutputTextBox.ScrollToEnd();

            Title = Properties.Resources.Title;
        }

        private async void Start_Work()
        {
            int licenseToDo = license.SelectedIndex;
            string cidToDo = cid.Text;
            string dklicToDo = dklic.Text;
            int formatToDo = format.SelectedIndex;
            int typeToDo = type.SelectedIndex;
            string inputToDo = input.Text;
            string outputToDo = output.Text;
            byte dklicModeToDo = (byte)dklicMode.SelectedIndex;
            byte dataToDo = (byte)data.SelectedIndex;
            int versionToDo = version.SelectedIndex;
            byte compressToDo = (byte)compress.SelectedIndex;
            string blockToDo = block.SelectionBoxItem.ToString();

            int modeToDo = mode.SelectedIndex;
            string sourceToDo = source.Text;
            int taskToDo = task.SelectedIndex;

            await Task.Run(() =>
            {
                if (!File.Exists(BIN_KLIC))
                    using (File.Create(BIN_KLIC)) { }

                WriteOutput("Operation started." + Environment.NewLine + Environment.NewLine, false);

                if (taskToDo == 0)
                {
                    if (File.Exists(inputToDo))
                    {
                        byte[] klicRaw = dklic_finder.GetKlic(inputToDo, TXT_KLIC);

                        if (klicRaw != null)
                        {
                            File.WriteAllBytes(BIN_KLIC, klicRaw);

                            WriteOutput("dev_klic: " + BitConverter.ToString(klicRaw).Replace("-", "") + Environment.NewLine + Environment.NewLine);
                        }
                        else
                        {
                            byte bruteforceMode = (byte)(modeToDo == 3 ? 3 : modeToDo);

                            string result = string.Empty;
                            if (bruteforceMode != 3)
                                result = _make_npdata.Bruteforce(inputToDo, sourceToDo, bruteforceMode) + Environment.NewLine;
                            else
                                result = _make_npdata.Bruteforce(inputToDo, sourceToDo, 1) + Environment.NewLine;

                            if (!result.Contains("Found valid klic!") && bruteforceMode == 3)
                                result = _make_npdata.Bruteforce(inputToDo, sourceToDo, 2) + Environment.NewLine;

                            if (!result.Contains("Found valid klic!") && bruteforceMode == 3)
                                result = _make_npdata.Bruteforce(inputToDo, sourceToDo, 0) + Environment.NewLine;

                            WriteOutput(result);

                            if (result.Contains("Found valid klic!"))
                            {
                                string klic = BitConverter.ToString(File.ReadAllBytes(BIN_KLIC)).Replace("-", "");

                                string cidStr = string.Empty;

                                using (FileStream fs = new FileStream(inputToDo, FileMode.Open, FileAccess.Read, FileShare.Read, 4096, FileOptions.RandomAccess))
                                using (BinaryReader br = new BinaryReader(fs))
                                {
                                    fs.Seek(0x10, SeekOrigin.Begin);
                                    cidStr = Encoding.UTF8.GetString(br.ReadBytes(0x24));
                                }

                                using (StreamWriter sw = new StreamWriter(TXT_KLIC, true))
                                    sw.WriteLine(klic + " [" + cidStr + "] ");

                                WriteOutput("dev_klic: " + klic + Environment.NewLine + Environment.NewLine);
                            }
                        }
                    }
                    else
                    {
                        if (Directory.Exists(inputToDo))
                        {
                            byte bruteforceMode = (byte)(modeToDo == 3 ? 3 : modeToDo);

                            string result = string.Empty;

                            foreach (var file in Directory.EnumerateFiles(inputToDo, "*.edat", SearchOption.AllDirectories))
                            {
                                byte[] klicRaw = dklic_finder.GetKlic(file, TXT_KLIC);

                                if (klicRaw != null)
                                {
                                    File.WriteAllBytes(BIN_KLIC, klicRaw);

                                    WriteOutput(file + " dev_klic: " + BitConverter.ToString(klicRaw).Replace("-", "") + Environment.NewLine + Environment.NewLine);

                                    continue;
                                }

                                if (bruteforceMode != 3)
                                    result = _make_npdata.Bruteforce(file, sourceToDo, bruteforceMode) + Environment.NewLine;
                                else
                                    result = _make_npdata.Bruteforce(file, sourceToDo, 1) + Environment.NewLine;

                                if (!result.Contains("Found valid klic!") && bruteforceMode == 3)
                                    result = _make_npdata.Bruteforce(file, sourceToDo, 2) + Environment.NewLine;

                                if (!result.Contains("Found valid klic!") && bruteforceMode == 3)
                                    result = _make_npdata.Bruteforce(file, sourceToDo, 0) + Environment.NewLine;

                                WriteOutput(result);

                                if (result.Contains("Found valid klic!"))
                                {
                                    string klic = BitConverter.ToString(File.ReadAllBytes(BIN_KLIC)).Replace("-", "");

                                    string cidStr = string.Empty;

                                    using (FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read, 4096, FileOptions.RandomAccess))
                                    using (BinaryReader br = new BinaryReader(fs))
                                    {
                                        fs.Seek(0x10, SeekOrigin.Begin);
                                        cidStr = Encoding.UTF8.GetString(br.ReadBytes(0x24));
                                    }

                                    using (StreamWriter sw = new StreamWriter(TXT_KLIC, true))
                                        sw.WriteLine(klic + " [" + cidStr + "] ");

                                    WriteOutput(file + " dev_klic: " + klic + Environment.NewLine + Environment.NewLine);
                                }
                            }
                        }
                    }
                }
                else if (taskToDo == 1)
                {
                    if (File.Exists(inputToDo))
                        Decryption(inputToDo, dklicModeToDo, dklicToDo);
                    else
                        foreach (var file in Directory.EnumerateFiles(inputToDo, "*", SearchOption.AllDirectories).Where(x => x.ToLower().EndsWith(".edat") || x.ToLower().EndsWith(".sdat")))
                            Decryption(file, dklicModeToDo, dklicToDo);
                }
                else if (taskToDo == 2)
                {
                    // The user selected SDAT format and Version 1
                    if (formatToDo == 1 && versionToDo == 0)
                    {
                        MessageBox.Show(Properties.Resources.SDATVer1Err, Properties.Resources.Title, MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }

                    if (File.Exists(inputToDo))
                        Encryption(inputToDo, licenseToDo, cidToDo, dklicToDo, formatToDo, typeToDo, inputToDo, outputToDo, dklicModeToDo, dataToDo, versionToDo.ToString(), compressToDo, blockToDo);
                    else
                        foreach (var file in Directory.EnumerateFiles(inputToDo, "*.dat", SearchOption.AllDirectories))
                            Encryption(file, licenseToDo, cidToDo, dklicToDo, formatToDo, typeToDo, inputToDo, outputToDo, dklicModeToDo, dataToDo, versionToDo.ToString(), compressToDo, blockToDo);
                }
                else if (taskToDo == 3)
                {
                    if (File.Exists(inputToDo))
                        Reencryption(inputToDo, licenseToDo, cidToDo, dklicToDo, formatToDo, typeToDo, inputToDo, outputToDo, dklicModeToDo, dataToDo, versionToDo.ToString(), compressToDo, blockToDo);
                    else
                        foreach (var file in Directory.EnumerateFiles(inputToDo, "*", SearchOption.AllDirectories).Where(x => x.ToLower().EndsWith(".edat") || x.ToLower().EndsWith(".sdat")))
                            Reencryption(file, licenseToDo, cidToDo, dklicToDo, formatToDo, typeToDo, inputToDo, outputToDo, dklicModeToDo, dataToDo, versionToDo.ToString(), compressToDo, blockToDo);

                    WriteOutput(Properties.Resources.ReencryptComplete + Environment.NewLine);
                }
                else
                    MessageBox.Show(Properties.Resources.ErrUnknownMode, Properties.Resources.Title, MessageBoxButton.OK, MessageBoxImage.Stop);

                WriteOutput("Operation finished.");
            });
        }

        private void WriteOutput(string message, bool append = true)
        {
            if (append)
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    OutputTextBox.AppendText(message);
                    OutputTextBox.ScrollToLine(OutputTextBox.LineCount - 1);
                }));
            else
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    OutputTextBox.Text = message;
                }));
        }

        private void Reencryption(string encryptedFile, int licenseToDo, string cidToDo, string dklicToDo, int formatToDo, int typeToDo,
                                  string inputToDo, string outputToDo, byte dklicModeToDo, byte dataToDo, string versionToDo, byte compressToDo, string blockToDo)
        {
            if (encryptedFile.Contains(".org"))
                encryptedFile = encryptedFile.Replace(".org", "");

            Decryption(encryptedFile, dklicModeToDo, dklicToDo);

            if (File.Exists(encryptedFile + ".dat"))
            {
                File.Move(encryptedFile, encryptedFile.Insert(encryptedFile.Length - 5, ".org"));

                Encryption(encryptedFile + ".dat", licenseToDo, cidToDo, dklicToDo, formatToDo, typeToDo, inputToDo, outputToDo, dklicModeToDo, dataToDo, versionToDo, compressToDo, blockToDo);

                File.Delete(encryptedFile + ".dat");
                File.Delete(encryptedFile.Insert(encryptedFile.Length - 5, ".org"));
            }
            else
            {
                byte[] tmp = dklic_finder.GetKlic(encryptedFile, TXT_KLIC);
                if (tmp == null)
                    WriteOutput(string.Format(Properties.Resources.ErrMissingKLic, encryptedFile) + Environment.NewLine + Environment.NewLine);
            }
        }

        private void Encryption(string decryptedFile, int licenseToDo, string cidToDo, string dklicToDo, int formatToDo, int typeToDo,
                                string inputToDo, string outputToDo, byte dklicModeToDo, byte dataToDo, string versionToDo, byte compressToDo, string blockToDo)
        {
            string rap = licenseToDo + 1 == 2 ? RAPS_FOLDER + cidToDo + ".rap" : string.Empty;
            string cidStr = cidToDo;
            string klicStr = dklicToDo;

            byte? formatValue = null;
            if (formatToDo == 0)
                formatValue = 1;
            else if (formatToDo == 1)
                formatValue = 0;

            byte? typeValue = null;
            switch (typeToDo)
            {
                case 0:
                    typeValue = 0;
                    break;

                case 1:
                    typeValue = 1;
                    break;

                case 2:
                    typeValue = 20;
                    break;

                case 3:
                    typeValue = 21;
                    break;

                case 4:
                    typeValue = 30;
                    break;
            }

            string outputFile = string.Empty;

            if (File.Exists(inputToDo))
                outputFile = outputToDo;
            else
                outputFile = decryptedFile.Remove(decryptedFile.Length - 4);

            string originalFile = outputFile.Insert(outputFile.Length - 5, ".org");

            if (File.Exists(originalFile))
            {
                byte[] klicRaw = dklic_finder.GetKlic(originalFile, TXT_KLIC);

                if (klicRaw != null)
                    klicStr = BitConverter.ToString(klicRaw, 0).Replace("-", "");

                using (FileStream fs = new FileStream(originalFile, FileMode.Open, FileAccess.Read, FileShare.Read, 4096, FileOptions.RandomAccess))
                {
                    byte[] cidRaw = new byte[0x24];

                    fs.Position = 0x10;
                    fs.Read(cidRaw, 0, cidRaw.Length);

                    cidStr = Encoding.UTF8.GetString(cidRaw);
                }

                if (cidToDo.EndsWith("*"))
                {
                    string tmp = cidToDo.Replace("*", "");

                    cidStr = cidStr.Remove(0, tmp.Length);
                    cidStr = cidStr.Insert(0, tmp);
                }
            }

            WriteOutput(_make_npdata.Encrypt(decryptedFile, outputFile, cidStr, dklicModeToDo, klicStr, rap,
                                                      formatValue.Value, dataToDo, (byte)(byte.Parse(versionToDo) + 1), compressToDo,
                                                      byte.Parse(blockToDo), (byte)(licenseToDo + 1), typeValue.Value) + Environment.NewLine);
        }

        private void Decryption(string encryptedFile, byte dklicModeToDo, string klicStr)
        {
            string rap = string.Empty;

            if (!encryptedFile.ToLower().EndsWith(".sdat"))
            {
                rap = Retrieve_RAP(encryptedFile);

                if (!File.Exists(rap))
                {
                    bool free = Check_EDAT_License(encryptedFile);

                    if (!free)
                    {
                        WriteOutput(string.Format("{0}{1}{1}", string.Format(Properties.Resources.ErrRAPNotFound, rap), Environment.NewLine));
                        return;
                    }
                    else
                        rap = string.Empty;
                }

                byte[] klic = dklic_finder.GetKlic(encryptedFile, TXT_KLIC);

                if (klic != null)
                    klicStr = BitConverter.ToString(klic).Replace("-", "");
            }

            WriteOutput(_make_npdata.Decrypt(encryptedFile, encryptedFile + ".dat", dklicModeToDo, klicStr, rap) + Environment.NewLine);
        }

        private bool Check_EDAT_License(string EDATfile)
        {
            int licenseType;

            using (FileStream stream = new FileStream(EDATfile, FileMode.Open, FileAccess.Read, FileShare.Read, 4096, FileOptions.RandomAccess))
            {
                stream.Position = 0xB;
                licenseType = stream.ReadByte();
            }

            if (licenseType == 3)
                return true;
            else
                return false;
        }

        private string Retrieve_RAP(string encryptedFile)
        {
            byte[] cidRaw = new byte[36];

            using (FileStream stream = new FileStream(encryptedFile, FileMode.Open, FileAccess.Read, FileShare.Read, 4096, FileOptions.RandomAccess))
            {
                stream.Position = 0x10;
                stream.Read(cidRaw, 0, cidRaw.Length);
            }

            string cid = Encoding.UTF8.GetString(cidRaw);

            string rapFile = RAPS_FOLDER + cid + ".rap";

            return rapFile;
        }

        private string Read_RAP(string rapFile)
        {
            byte[] rapValueRaw = File.ReadAllBytes(rapFile);

            string rapValue = BitConverter.ToString(rapValueRaw).Replace("-", "");

            return rapValue;
        }

        private void task_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (task.SelectedIndex == 2 && cid.Text.Length != 36)
                startButton.IsEnabled = false;
            else
                startButton.IsEnabled = true;

            // Disable unrelated elements
            if (task.SelectedIndex == 0) // Bruteforcing
            {
                source.IsEnabled = true;
                sourceBtn.IsEnabled = true;
                output.IsEnabled = false;
                outputBtn.IsEnabled = false;
                cid.IsEnabled = false;

                dklicMode.IsEnabled = false;
                dklic.IsEnabled = false;

                format.IsEnabled = false;
                data.IsEnabled = false;
                version.IsEnabled = false;
                compress.IsEnabled = false;

                block.IsEnabled = false;
                license.IsEnabled = false;
                type.IsEnabled = false;
                mode.IsEnabled = true;
            }
            else if (task.SelectedIndex == 1) // Decrypting
            {
                source.IsEnabled = false;
                sourceBtn.IsEnabled = false;
                output.IsEnabled = true;
                outputBtn.IsEnabled = true;
                cid.IsEnabled = false;

                dklicMode.IsEnabled = true;
                dklic.IsEnabled = dklicMode.SelectedIndex != 0 ? true : false;

                format.IsEnabled = false;
                data.IsEnabled = false;
                version.IsEnabled = false;
                compress.IsEnabled = false;

                block.IsEnabled = false;
                license.IsEnabled = false;
                type.IsEnabled = false;
                mode.IsEnabled = false;
            }
            else if (task.SelectedIndex == 2) // Encrypting
            {
                source.IsEnabled = false;
                sourceBtn.IsEnabled = false;
                output.IsEnabled = true;
                outputBtn.IsEnabled = true;
                cid.IsEnabled = true;

                dklicMode.IsEnabled = true;
                dklic.IsEnabled = dklicMode.SelectedIndex != 0 ? true : false;

                format.IsEnabled = true;
                data.IsEnabled = true;
                version.IsEnabled = true;
                compress.IsEnabled = true;

                block.IsEnabled = true;
                license.IsEnabled = true;
                type.IsEnabled = true;
                mode.IsEnabled = false;
            }
            else if (task.SelectedIndex == 3) // Reencrypting
            {
                source.IsEnabled = false;
                sourceBtn.IsEnabled = false;
                output.IsEnabled = true;
                outputBtn.IsEnabled = true;
                cid.IsEnabled = true;

                dklicMode.IsEnabled = true;
                dklic.IsEnabled = dklicMode.SelectedIndex != 0 ? true : false;

                format.IsEnabled = true;
                data.IsEnabled = true;
                version.IsEnabled = true;
                compress.IsEnabled = true;

                block.IsEnabled = true;
                license.IsEnabled = true;
                type.IsEnabled = true;
                mode.IsEnabled = false;
            }
        }

        private void format_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (license == null)
                return;

            if (format.SelectedIndex == 0)
            {
                license.IsEnabled = true;
                type.IsEnabled = true;
                cid.IsEnabled = true;

                cid_TextChanged(null, null);
            }
            else
            {
                license.IsEnabled = false;
                type.IsEnabled = false;
                cid.IsEnabled = false;

                startButton.IsEnabled = true;
            }
        }

        private void cid_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            if (cid.Text.Length == 36 || task.SelectedIndex == 3)
                startButton.IsEnabled = true;
            else
                startButton.IsEnabled = false;
        }

        private void About_Click(object sender, RoutedEventArgs e)
        {
            Assembly thisAssembly = Assembly.GetExecutingAssembly();
            string versionInfo_make_npdata;

            using (Process p = new Process())
            {
                p.StartInfo = new ProcessStartInfo("make_npdata.exe");
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.CreateNoWindow = true;
                p.Start();

                using (StreamReader reader = p.StandardOutput)
                    versionInfo_make_npdata = reader.ReadToEnd();
            }

            versionInfo_make_npdata = Regex.Match(versionInfo_make_npdata, @"v\d\.\d\.\d", RegexOptions.IgnoreCase).Value;

            FileVersionInfo fvi_this = FileVersionInfo.GetVersionInfo(Assembly.GetEntryAssembly().Location);

            StringBuilder aboutMsg = new StringBuilder(string.Format("{0} by {1}{2}{2}", Properties.Resources.Title, fvi_this.CompanyName, Environment.NewLine));
            aboutMsg.Append(string.Format("{0} v{1}{2}{2}{3} {4}", Path.GetFileName(thisAssembly.GetName().CodeBase), thisAssembly.GetName().Version, Environment.NewLine, Path.GetFileName("make_npdata.exe"), versionInfo_make_npdata));

            if (File.Exists("Updater.exe"))
            {
                FileVersionInfo fvi_updater = FileVersionInfo.GetVersionInfo(Environment.CurrentDirectory + @"\Updater.exe");
                aboutMsg.Append(Environment.NewLine + Environment.NewLine + Path.GetFileName(fvi_updater.FileName) + " v" + fvi_updater.ProductVersion);
            }

            MessageBox.Show(aboutMsg.ToString(), Properties.Resources.About, MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void UpdateChk_Click(object sender, RoutedEventArgs e)
        {
            if (File.Exists("Updater.exe"))
            {
                Assembly thisAssembly = Assembly.GetExecutingAssembly();
                Process.Start("Updater.exe", string.Format("\"{0}\" \"{1}\" \"{2}\"", thisAssembly.GetName().Name, thisAssembly.GetName().Version, Process.GetCurrentProcess().MainModule.FileName));
            }
            else
                MessageBox.Show(Properties.Resources.ErrUpdaterNotFound, Properties.Resources.Title, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void Help_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(Properties.Resources.HelpMsg, Properties.Resources.Title, MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void Language_Checked(object sender, RoutedEventArgs e)
        {
            string header = (sender as MenuItem).Header.ToString();

            INIFile ini = new INIFile("settings.ini", false, true);
            string previousLanguage = ini.GetValue("General", "Language", "en");
            bool languageChanged = false;

            if (header == "Castellano" && previousLanguage != "es")
            {
                ini.SetValue("General", "Language", "es");
                languageChanged = true;
            }
            else if (header == "English" && previousLanguage != "en")
            {
                ini.SetValue("General", "Language", "en");
                languageChanged = true;
            }
            else if (header == "Polski" && previousLanguage != "pl")
            {
                ini.SetValue("General", "Language", "pl");
                languageChanged = true;
            }
            else if (header == "Pусский" && previousLanguage != "ru")
            {
                ini.SetValue("General", "Language", "ru");
                languageChanged = true;
            }

            if (languageChanged)
                App.UpdateGUILanguage();
        }

        private void dklicMode_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((sender as ComboBox) == null || dklic == null)
                return;

            if ((sender as ComboBox).SelectedIndex != 0)
                dklic.IsEnabled = true;
            else
                dklic.IsEnabled = false;
        }
    }
}
