﻿using System;
using System.IO;
using System.Numerics;
using System.Security.Cryptography;

namespace Shared_Classes
{
    class DKlic_Finder
    {
        public byte[] GetKlic(string edatFile, string devKlicsFile, bool klicsFileIsText = true)
        {
            if (!File.Exists(devKlicsFile))
                return null;

            byte[] klic = null;
            byte[] npd = new byte[0x80];

            using (FileStream fs = new FileStream(edatFile, FileMode.Open, FileAccess.Read, FileShare.Read, 80, FileOptions.SequentialScan))
                fs.Read(npd, 0, npd.Length);

            if (klicsFileIsText)
                klic = CheckKlicsTxt(npd, devKlicsFile);
            else
                klic = CheckKlicsBin(npd, devKlicsFile);

            return klic;
        }

        private static byte[] CheckKlicsBin(byte[] npd, string devKlicsFile)
        {
            byte[] klic = new byte[0x10];
            bool foundKlic = false;

            using (BinaryReader reader = new BinaryReader(File.OpenRead(devKlicsFile)))
            {
                int readBytesCount;

                while ((readBytesCount = reader.Read(klic, 0, klic.Length)) > 0)
                {
                    foundKlic = CheckNPDHash2(klic, npd);

                    if (foundKlic)
                        break;
                }
            }

            if (foundKlic)
                return klic;
            else
                return null;
        }

        private static byte[] CheckKlicsTxt(byte[] npd, string devKlicsFile)
        {
            byte[] klic = null;
            bool foundKlic = false;

            using (StreamReader reader = new StreamReader(devKlicsFile))
            {
                string line;

                while ((line = reader.ReadLine()) != null)
                {
                    if (string.IsNullOrWhiteSpace(line))
                        continue;

                    string klicRaw = line.Substring(0, 32);
                    klic = ConversionUtils.GetByteArray(klicRaw);

                    foundKlic = CheckNPDHash2(klic, npd);
                    
                    if (foundKlic)
                        break;
                }
            }

            if (foundKlic)
                return klic;
            else
                return null;
        }

        private static bool CheckNPDHash2(byte[] klicensee, byte[] npd)
        {
            byte[] output = new byte[0x10];
            
            XOR(output, klicensee, Utils.HexStringToByteArray("6BA52976EFDA16EF3C339FB2971E256B"));
            
            byte[] buffer2 = CMAC128(output, npd, 0, 0x60);

            return CompareBytes(buffer2, 0, npd, 0x60, 0x10);
        }

        private static bool CompareBytes(byte[] value1, int offset1, byte[] value2, int offset2, int len)
        {
            for (int i = 0; i < len; i++)
                if (value1[i + offset1] != value2[i + offset2])
                    return false;

            return true;
        }

        private static void XOR(byte[] output, byte[] inputA, byte[] inputB)
        {
            for (int i = 0; i < inputA.Length; i++)
                output[i] = (byte)(inputA[i] ^ inputB[i]);
        }

        private static byte[] CMAC128(byte[] key, byte[] i, int inOffset, int len)
        {
            byte[] buffer = new byte[0x10];
            byte[] buffer2 = new byte[0x10];
            byte[] dest = new byte[0x10];
            byte[] inputB = new byte[0x10];
            
            calculateSubkey(key, buffer, buffer2);
            
            int srcPos = inOffset;
            int length = len;
            
            while (length > 0x10)
            {
                Array.Copy(i, srcPos, dest, 0L, 0x10);
                XOR(dest, dest, inputB);
                aesecbEncrypt(key, dest, 0, inputB, 0, dest.Length);
                srcPos += 0x10;
                length -= 0x10;
            }

            dest = new byte[0x10];
            
            Array.Copy(i, srcPos, dest, 0L, length);
            
            if (length == 0x10)
            {
                XOR(dest, dest, inputB);
                XOR(dest, dest, buffer);
            }
            else
            {
                dest[length] = 0x80;
                XOR(dest, dest, inputB);
                XOR(dest, dest, buffer2);
            }

            aesecbEncrypt(key, dest, 0, inputB, 0, dest.Length);

            return inputB;
        }

        private static void calculateSubkey(byte[] key, byte[] K1, byte[] K2)
        {
            byte[] i = new byte[0x10];
            byte[] o = new byte[0x10];
            
            aesecbEncrypt(key, i, 0, o, 0, i.Length);
            
            BigInteger integer = new BigInteger(ConversionUtils.ReverseByteWithSizeFIX(o));
            
            if ((o[0] & 0x80) != 0)
                integer = (integer << 1) ^ new BigInteger(0x87);
            else
                integer = integer << 1;

            byte[] src = ConversionUtils.ReverseByteWithSizeFIX(integer.ToByteArray());

            if (src.Length >= 0x10)
                Array.Copy(src, src.Length - 0x10, K1, 0L, 0x10);
            else
            {
                Array.Copy(i, 0, K1, 0L, i.Length);
                Array.Copy(src, 0, K1, (long)(0x10 - src.Length), src.Length);
            }

            integer = new BigInteger(ConversionUtils.ReverseByteWithSizeFIX(K1));
            
            if ((K1[0] & 0x80) != 0)
                integer = (integer << 1) ^ new BigInteger(0x87);
            else
                integer = integer << 1;

            src = ConversionUtils.ReverseByteWithSizeFIX(integer.ToByteArray());

            if (src.Length >= 0x10)
                Array.Copy(src, src.Length - 0x10, K2, 0L, 0x10);
            else
            {
                Array.Copy(i, 0, K2, 0L, i.Length);
                Array.Copy(src, 0, K2, (long)(0x10 - src.Length), src.Length);
            }
        }

        private static void aesecbEncrypt(byte[] key, byte[] i, int inOffset, byte[] o, int outOffset, int len)
        {
            CipherMode eCB = CipherMode.ECB;
            PaddingMode none = PaddingMode.None;
            int opMode = 1;
            crypto(key, eCB, none, null, opMode, i, inOffset, len, o, outOffset);
        }

        private static void crypto(byte[] key, CipherMode mode, PaddingMode padding, byte[] iv, int opMode, byte[] i, int inOffset, int len, byte[] o, int outOffset)
        {
            RijndaelManaged managed = new RijndaelManaged
            {
                Padding = padding,
                Mode = mode,
                KeySize = 0x80,
                BlockSize = 0x80,
                Key = key
            };
            if (iv != null)
            {
                managed.IV = iv;
            }
            byte[] src = null;

            src = managed.CreateEncryptor().TransformFinalBlock(i, inOffset, len);

            Array.Copy(src, 0, o, (long)outOffset, len);
        }
    }
}
