﻿using System.Diagnostics;
using System.IO;

namespace Make_npdata_GUI.Model
{
    internal class Make_npdata
    {
        private string @params;
        private string _stdout;
        public string Params { get { return @params; } private set { @params = value; } }
        public string Stdout { get { return _stdout; } private set { _stdout = value; } }

        public string Bruteforce(string input, string source, byte mode = 1)
        {
            Params = string.Format("-b \"{0}\" \"{1}\" {2}", input, source, mode);

            using (Process p = new Process())
            {
                p.StartInfo = new ProcessStartInfo("make_npdata.exe", Params);
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.CreateNoWindow = true;
                p.Start();

                using (StreamReader reader = p.StandardOutput)
                    Stdout = reader.ReadToEnd().Replace("klic.bin", "klics.txt");
            }

            return Stdout;
        }

        public string Decrypt(string input, string output, byte klicMode, string klicStr, string rap)
        {
            string klic = string.Empty;

            if (klicMode == 8)
                klic = string.IsNullOrWhiteSpace(klicStr) ? klicMode.ToString() : string.Format("{0} {1}", klicMode, klicStr);

            Params = string.Format("-d \"{0}\" \"{1}\" {2} {3}", input, output, klic, rap);

            using (Process p = new Process())
            {
                p.StartInfo = new ProcessStartInfo("make_npdata.exe", Params);
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.CreateNoWindow = true;
                p.Start();

                using (StreamReader reader = p.StandardOutput)
                    Stdout = reader.ReadToEnd();
            }

            return Stdout;
        }

        public string Encrypt(string input, string output, string cid, byte klicMode, string klicStr, string rap, byte format = 1, byte data = 1, byte version = 3, byte compress = 0, byte block = 16, byte license = 3, byte type = 0)
        {
            string klic = string.Empty;

            if (klicMode == 8)
                klic = string.IsNullOrWhiteSpace(klicStr) ? klicMode.ToString() : string.Format("{0} {1}", klicMode, klicStr);

            Params = string.Format("-e \"{0}\" \"{1}\" {2} {3} {4} {5} {6} {7} {8} {9} {10} {11}", input, output, format, data, version, compress, block, license, type, cid, klic, rap);

            using (Process p = new Process())
            {
                p.StartInfo = new ProcessStartInfo("make_npdata.exe", Params);
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.CreateNoWindow = true;
                p.Start();

                using (StreamReader reader = p.StandardOutput)
                    Stdout = reader.ReadToEnd();
            }

            return Stdout;
        }
    }
}
